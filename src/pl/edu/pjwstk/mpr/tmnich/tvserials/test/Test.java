package pl.edu.pjwstk.mpr.tmnich.tvserials.test;
import java.sql.SQLException;
import java.util.ArrayList;

import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Actor;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Director;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Episode;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Season;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.TvSeries;
import pl.edu.pjwstk.mpr.tmnich.tvserials.service.TvSeriesManager;

public class Test {

	public static void main(String[] args) {
		

		// Utworzenie obiektu, kt�ry automatycznie nawi�zuje po��czenie z baz� danych i posiada metody operuj�ce na niej
		
		
		
		// Zak�adanie tabeli je�eli nie ma jej w bazie
		//if (! myBase.tableTest("Seasons")) {myBase.executeUpdate("CREATE TABLE Seasons (id bigint AUTO_INCREMENT PRIMARY KEY, seasonNumber int, yearOfRelease bigint)");}
		//if (! myBase.tableTest("Actors")) {myBase.executeUpdate("CREATE TABLE Actors (id bigint AUTO_INCREMENT PRIMARY KEY, name varchar(20), biography TEXT, dateOfBirth date)");}
		//if (! myBase.tableTest("Directors")) {myBase.executeUpdate("CREATE TABLE Directors (id bigint AUTO_INCREMENT PRIMARY KEY, name varchar(20), biography TEXT, dateOfBirth date)");}
		//if (! myBase.tableTest("Episodes")) {myBase.executeUpdate("CREATE TABLE Episodes (id bigint AUTO_INCREMENT PRIMARY KEY, name varchar(20), episodeNumber int, duration TIME, releaseDate date)");}
		
		
		// utworzenie przyk�adowego serialu i zapisanie go w bazie
		String biographyPM = "Porzuci� prawo po pierwszym roku, kiedy to uzna�, �e jego prawdziw� mi�o�ci� jest aktorstwo. Rozpocz�� wi�c studia na wroc�awskiej PWST. Obecnie mo�na ogl�da� go na deskach warszawskiego teatru \"Kwadrat\". Zagra� tam w sztukach: \"S�uga dw�ch Pan�w\", \"Szalone no�yczki\", \"Wszystko w rodzinie\", \"Nie teraz kochanie\". Obok aktorstwa ogromn� pasj� Paw�a jest muzyka, za�o�y� nawet sw�j w�asny zesp�, w kt�rym �piewa i pisze teksty.";
		String biographyPV = "Zdawa� do ��dzkiej Szko�y Filmowej, do kt�rej jednak si� nie dosta�. Absolwent socjologii w Collegium Civitas w Warszawie. Obroni� prac� magistersk� na kanwie serialu dokumentalnego Prawdziwe psy, kt�rego by� wsp�scenarzyst� i re�yserem. Serial opowiada� o pracy warszawskiej policji. Wsp�autor (z Ann� Kowalsk� i Rafa�em G�ogowskim) przekrojowej pracy socjologicznej Polski ruch feministyczny � analiza ruchu spo�ecznego wg teorii Alaina Touraine�a.";

		TvSeries pitbull = new TvSeries ("Pitbull", new Director("Patryk Vega", "1977/01/02", biographyPV ));
		pitbull.addActor(new Actor("Pawe� Ma�aszy�ski", "1976/06/26", biographyPM));
		Season pitbullS1 = new Season(1, 2005);
		pitbullS1.addEpisode(new Episode("Odcinek 1.", "2005/12/15", 1, 50));
		pitbullS1.addEpisode(new Episode("Odcinek 2.", "2005/12/21", 2, 50));
		pitbullS1.addEpisode(new Episode("Odcinek 3.", "2005/12/21", 3, 50));
		pitbullS1.addEpisode(new Episode("Odcinek 4.", "2005/12/28", 4, 50));
		pitbullS1.addEpisode(new Episode("Odcinek 5.", "2005/12/29", 5, 50));
		pitbull.addSeason(pitbullS1);

		TvSeriesManager TvSeries2db = new TvSeriesManager(); // obiekt nawi�zuj�cy po��czenie
		TvSeries2db.addCompleteTvSeries(pitbull); // zapisanie przyk�adowego serialu w bazie
		try {
			ArrayList<TvSeries> allSeries = TvSeries2db.getAllTvSeriesFromDB(); // odczytanie wszystkich seriali
			TvSeries2db.addCompleteTvSeries(allSeries.get(0)); // ponowne zapisanie w bazie pierwszego z listy (je�eli baza by�a pusta, to mam teraz dwie kopie serialu)
			TvSeries2db.deleteTvSeriesFromDB(1);
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		
	}

}
