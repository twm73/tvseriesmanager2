package pl.edu.pjwstk.mpr.tmnich.tvserials.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public abstract class Manager {

	protected Connection conn = null;
	
	protected String url =  "jdbc:mysql://31.179.186.242:3306/TVSeriesManager";
	protected String user =  "tomek";
	protected String pass =  "fsd24KIHda3432";
	
	protected PreparedStatement addStmt;
	protected PreparedStatement deleteAllStmt;
	protected PreparedStatement deleteStmt;
	protected PreparedStatement getAllStmt;
	protected PreparedStatement getStmt;

	
	protected Statement statement;
	protected ResultSet data = null;
	
	public Manager (){
		try {
			conn = DriverManager.getConnection(url, user, pass);
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		
	}
	
	public boolean tableTest(String tableName) { // sprawdzenie czy jest tablica o zadanej nazwie
		boolean flag = false;
		ResultSet metaData = null;
		try {
			metaData = conn.getMetaData().getTables(null, null, null, null);
			while (metaData.next()) {
				if (tableName.equalsIgnoreCase(metaData.getString("TABLE_NAME"))) {
					flag = true;
					break;
				}
			}
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return flag;
	}

	public void executeQuery(String query) { // wykonanie polecenia
		try {
			Statement tempStatement = conn.createStatement();
			data = tempStatement.executeQuery(query);
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	
	public void executeUpdate(String query) { // wykonanie polecenia
		try {
			Statement tempStatement = conn.createStatement();
			tempStatement.executeUpdate(query);
			System.out.println("Uda�o si� wykona� polecenie \"" + query + "\".");
			
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		
	}	
	
}
