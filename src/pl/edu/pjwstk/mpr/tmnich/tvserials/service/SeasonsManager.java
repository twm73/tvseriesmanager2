package pl.edu.pjwstk.mpr.tmnich.tvserials.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Episode;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Season;

public class SeasonsManager extends Manager {

	public SeasonsManager() {
		super();
		try {
			if (!this.tableTest("Seasons")) {
				this.executeUpdate(
						"CREATE TABLE Seasons (id bigint AUTO_INCREMENT PRIMARY KEY, tvSeriesId bigint, seasonNumber int, yearOfRelease bigint, FOREIGN KEY (tvSeriesId) REFERENCES TvSeries(id))");
			}

			addStmt = conn.prepareStatement("INSERT INTO Seasons (seasonNumber, yearOfRelease, tvSeriesId) VALUES (?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			deleteAllStmt = conn.prepareStatement("DELETE FROM Seasons");
			deleteStmt = conn.prepareStatement("DELETE FROM Seasons WHERE tvSeriesId=?");
			getAllStmt = conn.prepareStatement("SELECT id, seasonNumber, yearOfRelease, tvSeriesId FROM Seasons");
			getStmt = conn.prepareStatement("SELECT id, seasonNumber, yearOfRelease FROM Seasons WHERE tvSeriesId=?");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	public void addCompleteSeason(Season season, long tvSeriesId) throws SQLException  {
		try {
		this.addSeason(season, tvSeriesId);
		EpisodsManager episodsManager = new EpisodsManager();
		for (Episode episode: season.getEpisodes()){episodsManager.addEpisode(episode, season.getId());}
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public void addSeason(Season season, long tvSeriesId) throws SQLException  {
		addStmt = conn.prepareStatement("INSERT INTO Seasons (seasonNumber, yearOfRelease, tvSeriesId) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
		addStmt.setInt(1, season.getSeasonNumber());
		addStmt.setInt(2, season.getYearOfRelease());
		addStmt.setLong(3, tvSeriesId);
		int affectedRows = addStmt.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Nie uda�o sie doda� sezonu.");
        }

        try (ResultSet generatedKeys = addStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
            	season.setId(generatedKeys.getLong(1));
            	
            }
            else {
                throw new SQLException("B��d w dodawaniu sezonu do bazy, nie otrzymano ID.");
            }
        }			
		
	}
    public ArrayList<Season> getSeasonsFromDB(long tvSeriesId) throws SQLException{
    	ArrayList<Season> seasons = new ArrayList<Season>();
    	getStmt.setLong(1, tvSeriesId);
    	ResultSet rs = getStmt.executeQuery();
    	while (rs.next()) {
    		Season season = new Season(rs.getInt("id"), rs.getInt("seasonNumber"), rs.getInt("yearOfRelease"));
    		season.setEpisodes(new EpisodsManager().getEpisodsFromDB(season.getSeasonNumber()));
    		seasons.add(season);
    	}
    	return seasons;
    }
    public void deleteSeasonFromDB(long tvSeriesId){
		try {
			getStmt.setLong(1, tvSeriesId);
	    	ResultSet rs = getStmt.executeQuery();
	    	while (rs.next()) {
	    		new EpisodsManager().deleteEpisodsFromDB(rs.getInt("id")); // kasowanie wszystkich odcink�w powi�zanych z ID wskazanego sezonu
	    	}			
	    	deleteStmt.setLong(1, tvSeriesId); // kasowanie wszystkich sezon�w powi�zanych z ID wskazanego serialu
	    	deleteStmt.executeUpdate();
	    	System.out.println("Usuni�to sezon.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}  
}
