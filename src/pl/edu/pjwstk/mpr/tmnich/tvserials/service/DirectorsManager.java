package pl.edu.pjwstk.mpr.tmnich.tvserials.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Director;

public class DirectorsManager extends Manager {

	public DirectorsManager() {
		super();

		try {
			if (! this.tableTest("Directors")) {this.executeUpdate("CREATE TABLE Directors (id bigint AUTO_INCREMENT PRIMARY KEY, name varchar(20), biography TEXT, dateOfBirth date)");}			
			addStmt = conn.prepareStatement("INSERT INTO Directors (name, biography, dateOfBirth) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			deleteAllStmt = conn.prepareStatement("DELETE FROM Directors");
			getAllStmt = conn.prepareStatement("SELECT id, name, biography, dateOfBirth FROM Directors");
			getStmt = conn.prepareStatement("SELECT id, name, biography, dateOfBirth FROM Directors WHERE id= ?");
			deleteStmt = conn.prepareStatement("DELETE FROM Directors WHERE id= ?");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}

	}
	public void addDirector(Director director) throws SQLException {
		
		java.sql.Date date = java.sql.Date.valueOf(director.getdateOfBirth());
		addStmt.setString(1, director.getName());
		addStmt.setString(2, director.getBiography());
		addStmt.setDate(3, date);
		
		int affectedRows = addStmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Nie uda�o sie doda� re�ysera.");
        }

        try (ResultSet generatedKeys = addStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
            	director.setId(generatedKeys.getLong(1));
            }
            else {
                throw new SQLException("B��d w dodawaniu re�ysera do bazy, nie otrzymano ID.");
            }
        }		
	}	
	void clearDirectors() {
		try {
			deleteAllStmt.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	void deleteDirector(long id) {
		try {
			deleteStmt.setLong(1, id);
			deleteStmt.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}		
    public Director getDirectorFromDB(long id) throws SQLException{
    	getStmt.setLong(1, id);
    	ResultSet rs = getStmt.executeQuery();
    	rs.next();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	Director director = new Director(rs.getInt("id"), rs.getString("name"), LocalDate.parse(rs.getString("dateOfBirth"), formatters),rs.getString("biography"));
    	return director;
    }
}
