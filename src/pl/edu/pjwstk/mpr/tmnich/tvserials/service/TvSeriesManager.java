package pl.edu.pjwstk.mpr.tmnich.tvserials.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Actor;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Season;
import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.TvSeries;

public class TvSeriesManager extends Manager {
	
	protected PreparedStatement actors4TvSeries;
	protected PreparedStatement addIdActors4TvSeries;
	protected PreparedStatement deleteActors4TvSeries;

	public TvSeriesManager() {
		super();
		try {
		addStmt = conn.prepareStatement("INSERT INTO TvSeries (directorId, name) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
		deleteAllStmt = conn.prepareStatement("DELETE FROM TvSeries");
		deleteStmt = conn.prepareStatement("DELETE FROM TvSeries WHERE id=?");
		getAllStmt = conn.prepareStatement("SELECT id FROM TvSeries");
		getStmt = conn.prepareStatement("SELECT id, name, directorId FROM TvSeries WHERE id=?");
		actors4TvSeries = conn.prepareStatement("SELECT idActor FROM Actors4TvSeries WHERE idTvSeries=?");
		deleteActors4TvSeries = conn.prepareStatement("DELETE FROM Actors4TvSeries WHERE idTvSeries=?");
		
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	public void addCompleteTvSeries(TvSeries tvseries){
		
		try {
			ActorsManager actorManager = new ActorsManager();	
			for (Actor actor: tvseries.getActors()){actorManager.addActor(actor);}
			DirectorsManager directorManager = new DirectorsManager();
			directorManager.addDirector(tvseries.getDirector());
			if (! this.tableTest("TvSeries")) {this.executeUpdate("CREATE TABLE TvSeries (id bigint AUTO_INCREMENT PRIMARY KEY, directorId bigint, name varchar(20), FOREIGN KEY (directorId) REFERENCES Directors(id))");}
			this.addTvSeries(tvseries);
			
			if (! this.tableTest("Actors4TvSeries")) {this.executeUpdate("CREATE TABLE Actors4TvSeries (idActor bigint, idTvSeries bigint, FOREIGN KEY (idActor) REFERENCES Actors(id), FOREIGN KEY (idTvSeries) REFERENCES TvSeries(id))");}
			for (Actor actor: tvseries.getActors()){
				addIdActors4TvSeries = conn.prepareStatement("INSERT INTO Actors4TvSeries (idActor, idTvSeries) VALUES (?, ?)");

				  
				addIdActors4TvSeries.setLong(1, actor.getId());
				addIdActors4TvSeries.setLong(2, tvseries.getId());
				addIdActors4TvSeries.executeUpdate();
			}
			SeasonsManager seasonsManager = new SeasonsManager();
			for (Season season: tvseries.getSeasons()){seasonsManager.addCompleteSeason(season, tvseries.getId());}
		
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public void addTvSeries(TvSeries tvSeries) throws SQLException {
		
		addStmt.setLong(1, tvSeries.getDirector().getId());
		addStmt.setString(2, tvSeries.getName());
		int affectedRows = addStmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Nie uda�o sie doda� serialu.");
        }

        try (ResultSet generatedKeys = addStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
            	tvSeries.setId(generatedKeys.getLong(1));
            }
            else {
                throw new SQLException("B��d w dodawaniu serialu do bazy, nie otrzymano ID.");
            }
        }		
	}	
    public TvSeries getTvSeriesFromDB(long tvSeriesId) throws SQLException{
    	getStmt.setLong(1, tvSeriesId);
    	ResultSet rs = getStmt.executeQuery(); // pobranie z bazy serialu o wskazanym ID
    	rs.next();
    	TvSeries tvSeries = new TvSeries(rs.getInt("id"), rs.getString("name"));
    	tvSeries.setDirector(new DirectorsManager().getDirectorFromDB(rs.getLong("directorId")));
    	actors4TvSeries.setLong(1, tvSeriesId);
    	ResultSet relationRS = actors4TvSeries.executeQuery(); // pobranie z tabeli pomocniczej ID aktor�w do serialu o wskazanym ID
    	while (relationRS.next()) {
    		tvSeries.addActor(new ActorsManager().getActorFromDB(relationRS.getLong("idActor"))); // dodanie do serialu kolejnych aktor�w
    	}
    	tvSeries.setSeasons(new SeasonsManager().getSeasonsFromDB(tvSeriesId)); // dodanie sezonu wraz z epizodami
    	return tvSeries;
    }
    public ArrayList<TvSeries> getAllTvSeriesFromDB() throws SQLException{
    	ArrayList<TvSeries> tvSeries = new ArrayList<TvSeries>();
    	ResultSet rs = getAllStmt.executeQuery();
    	while (rs.next()) {
    		tvSeries.add(this.getTvSeriesFromDB(rs.getLong("id")));
    	}
    	return tvSeries;
    }
    public void deleteTvSeriesFromDB(long tvSeriesId) throws SQLException{
    	getStmt.setLong(1, tvSeriesId);
    	ResultSet rs = getStmt.executeQuery(); // pobranie z bazy serialu o wskazanym ID
    	rs.next();
    	
    	actors4TvSeries.setLong(1, tvSeriesId);
    	ResultSet relationRS = actors4TvSeries.executeQuery(); // pobranie z tabeli pomocniczej ID aktor�w do serialu o wskazanym ID
    	deleteActors4TvSeries.setLong(1, tvSeriesId); // kasowanie relacji z tabeli pomocniczej
    	deleteActors4TvSeries.executeUpdate();
    	ActorsManager actorsManager = new ActorsManager();
    	while (relationRS.next()) {
    		actorsManager.deleteActorFromDB(relationRS.getLong("idActor")); // kasowanie aktor�w zwi�zanych z serialem
    	}
    	new SeasonsManager().deleteSeasonFromDB(tvSeriesId);
    	deleteStmt.setLong(1, tvSeriesId); // kasowanie serialu
    	deleteStmt.executeUpdate();
    	System.out.println("Usuni�to serial.");
    	new DirectorsManager().deleteDirector(rs.getLong("directorId"));
    	System.out.println("Usuni�to re�ysera.");
    	
    	
    	
    }
}
