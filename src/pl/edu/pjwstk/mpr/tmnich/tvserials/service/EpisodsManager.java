package pl.edu.pjwstk.mpr.tmnich.tvserials.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Episode;

public class EpisodsManager extends Manager {

	public EpisodsManager() {
		super();
		try {
			if (!this.tableTest("Episodes")) {
				this.executeUpdate(
						"CREATE TABLE Episodes (id bigint AUTO_INCREMENT PRIMARY KEY, name varchar(20), seasonId bigint, episodeNumber int, duration int, releaseDate date, FOREIGN KEY (seasonId) REFERENCES Seasons(id))");
			}
			addStmt = conn.prepareStatement("INSERT INTO Episodes (name, episodeNumber, duration, releaseDate, seasonId) VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			deleteAllStmt = conn.prepareStatement("DELETE FROM Episodes");
			getAllStmt = conn.prepareStatement("SELECT id, name, episodeNumber, duration, releaseDate, seasonId FROM Episodes");
			getStmt = conn.prepareStatement("SELECT id, name, episodeNumber, duration, releaseDate, seasonId FROM Episodes WHERE seasonId= ?");
			deleteStmt = conn.prepareStatement("DELETE FROM Episodes WHERE seasonId= ?");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	public void addEpisode(Episode episode, long seasonId) throws SQLException  {
		java.sql.Date date = java.sql.Date.valueOf(episode.getRelaseDate());
		addStmt.setString(1, episode.getName());
		addStmt.setInt(2, episode.getEpisodeNumber());
		addStmt.setInt(3, episode.getDuration());
		addStmt.setDate(4, date);
		addStmt.setLong(5, seasonId);
		int affectedRows = addStmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Nie uda�o sie doda� odcinka.");
        }

        try (ResultSet generatedKeys = addStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
            	episode.setId(generatedKeys.getLong(1));
            }
            else {
                throw new SQLException("B��d w dodawaniu odcinka do bazy, nie otrzymano ID.");
            }
        }
	}
	void deleteEpisode(long seasonId) {
		try {
			deleteStmt.setLong(1, seasonId);
			deleteStmt.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}	
    public ArrayList<Episode> getEpisodsFromDB(long seasonId) throws SQLException{
    	getStmt.setLong(1, seasonId);
    	ResultSet rs = getStmt.executeQuery();
    	ArrayList<Episode> episods = new ArrayList<Episode>();
    	while (rs.next()) {
    		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    		episods.add( new Episode(rs.getInt("id"), rs.getString("name"), LocalDate.parse(rs.getString("releaseDate"), formatters), rs.getInt("episodeNumber"), rs.getInt("duration")));	
    	}
    	return episods;
    }
    public void deleteEpisodsFromDB(long seasonId){
    	
		try {
	    	deleteStmt.setLong(1, seasonId);
	    	deleteStmt.executeUpdate();
	    	System.out.println("Usuni�to odcinki z sezonu.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}     
}
