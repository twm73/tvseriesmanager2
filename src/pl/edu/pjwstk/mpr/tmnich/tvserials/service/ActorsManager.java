package pl.edu.pjwstk.mpr.tmnich.tvserials.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import pl.edu.pjwstk.mpr.tmnich.tvserials.domain.Actor;

public class ActorsManager extends Manager {
	

	
	public ActorsManager(){
		super();
		try {
			if (! this.tableTest("Actors")) {this.executeUpdate("CREATE TABLE Actors (id bigint AUTO_INCREMENT PRIMARY KEY, name varchar(20), biography TEXT, dateOfBirth date)");}
			addStmt = conn.prepareStatement("INSERT INTO Actors (name, biography, dateOfBirth) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			deleteAllStmt = conn.prepareStatement("DELETE FROM Actors");
			deleteStmt = conn.prepareStatement("DELETE FROM Actors WHERE id= ?");
			getAllStmt = conn.prepareStatement("SELECT id, name, biography, dateOfBirth FROM Actors");
			getStmt = conn.prepareStatement("SELECT id, name, biography, dateOfBirth FROM Actors WHERE id= ?");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	public void addActor(Actor actor) throws SQLException {
		
		java.sql.Date date = java.sql.Date.valueOf(actor.getdateOfBirth());
		addStmt.setString(1, actor.getName());
		addStmt.setString(2, actor.getBiography());
		addStmt.setDate(3, date);
		
		int affectedRows = addStmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Nie uda�o sie doda� aktora.");
        }

        try (ResultSet generatedKeys = addStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
            	actor.setId(generatedKeys.getLong(1));
            }
            else {
                throw new SQLException("B��d w dodawaniu aktora do bazy, nie otrzymano ID.");
            }
        }		
	}
	
	void clearActors() {
		try {
			deleteAllStmt.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	void deleteActorFromDB(long id) {
		try {
			deleteStmt.setLong(1, id);
			deleteStmt.executeUpdate();
			System.out.println("Usuni�to aktora.");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}	
    public Actor getActorFromDB(long id) throws SQLException{
    	getStmt.setLong(1, id);
    	ResultSet rs = getStmt.executeQuery();
    	rs.next();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	Actor actor = new Actor(rs.getInt("id"), rs.getString("name"), LocalDate.parse(rs.getString("dateOfBirth"), formatters), rs.getString("biography"));
    	return actor;
    }
}