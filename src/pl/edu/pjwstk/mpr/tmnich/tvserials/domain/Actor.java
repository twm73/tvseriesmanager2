package pl.edu.pjwstk.mpr.tmnich.tvserials.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Actor {
	private long id;

	private String name;
	private LocalDate dateOfBirth;
	private String biography;

	public Actor(String name, String dateOfBirth, String biography) {
		super();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		this.dateOfBirth = LocalDate.parse(dateOfBirth, formatters);
		this.name = name;
		this.biography = biography;
	}
	public Actor(long id, String name, String dateOfBirth, String biography) {
		super();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		this.dateOfBirth = LocalDate.parse(dateOfBirth, formatters);
		this.id = id;
		this.name = name;
		this.biography = biography;
	}	
	public Actor(String name, LocalDate dateOfBirth, String biography) {
		super();
		this.dateOfBirth = dateOfBirth;
		this.name = name;
		this.biography = biography;
	}
	public Actor(long id, String name, LocalDate dateOfBirth, String biography) {
		super();
		this.id = id;
		this.dateOfBirth = dateOfBirth;
		this.name = name;
		this.biography = biography;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getdateOfBirth() {
		return dateOfBirth;
	}

	public void setdateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public void setdateOfBirth(String dateOfBirth) {
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		this.dateOfBirth = LocalDate.parse(dateOfBirth, formatters);
	}


	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	

}
