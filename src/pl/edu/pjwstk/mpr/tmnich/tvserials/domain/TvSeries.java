package pl.edu.pjwstk.mpr.tmnich.tvserials.domain;
import java.util.ArrayList;

public class TvSeries {
	private long id;
	private String name;
	private ArrayList<Season> seasons = new ArrayList<Season>();
	private Director director;
	private ArrayList<Actor> actors = new ArrayList<Actor>();

	public TvSeries(String name, Director director) {
		this.name = name;
		this.director = director;
	}
	public TvSeries(long id, String name) {
		this.name = name;
		this.id = id;
		}
	public TvSeries(long id, String name, Director director) {
		this.id = id;
		this.name = name;
		this.director = director;
	}
	
	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Season> getSeasons() {
		return seasons;
	}
	public void setSeasons(ArrayList<Season> seasons) {
		this.seasons = seasons;
	}
	public void addSeason(Season season) {
		this.seasons.add(season);
	}
	public Director getDirector() {
		return director;
	}
	public void setDirector(Director director) {
		this.director = director;
	}
	public ArrayList<Actor> getActors() {
		return actors;
	}	
	public void setActors(ArrayList<Actor> actors) {
		this.actors = actors;
	}
	public void addActor(Actor actors) {
		this.actors.add(actors);
	}
	public void setId(long id) {
		this.id = id;
	}
	

}
