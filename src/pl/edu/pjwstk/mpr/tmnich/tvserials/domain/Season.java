package pl.edu.pjwstk.mpr.tmnich.tvserials.domain;
import java.util.ArrayList;

public class Season {
	private long id;
	private int seasonNumber;
	private int yearOfRelease;
	private ArrayList<Episode> episodes = new ArrayList<Episode>();

	public Season(int seasonNumber, int yearOfRelease) {
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
	}
	public Season(long id, int seasonNumber, int yearOfRelease) {
		this.id = id;
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
	}

	public long getId() {
		return id;
	}

	public int getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public int getYearOfRelease() {
		return yearOfRelease;
	}

	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}

	public ArrayList<Episode> getEpisodes() {
		return episodes;
	}
	public void addEpisode(Episode episode) {
		this.episodes.add(episode);
	}
	public void setEpisodes(ArrayList<Episode> episodes) {
		this.episodes = episodes;
	}

	public void setId(long id) {
		this.id = id;
	}
	

}
