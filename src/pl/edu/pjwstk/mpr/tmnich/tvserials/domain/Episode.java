package pl.edu.pjwstk.mpr.tmnich.tvserials.domain;
//import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Episode {

	private long id;
	private String name;
	private LocalDate relaseDate;
	private int episodeNumber;
	private int duration;

	public Episode(String name, LocalDate relaseDate, int episodeNumber, int duration) {
		this.name = name;
		this.relaseDate = relaseDate;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}
	public Episode(long id, String name, LocalDate relaseDate, int episodeNumber, int duration) {
		this.id = id;
		this.name = name;
		this.relaseDate = relaseDate;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}
	public Episode(String name, String relaseDate, int episodeNumber, int duration) {
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		this.relaseDate = LocalDate.parse(relaseDate, formatters);		
		this.name = name;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}
	public Episode(long id, String name, String relaseDate, int episodeNumber, int duration) {
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		this.relaseDate = LocalDate.parse(relaseDate, formatters);
		this.id = id;
		this.name = name;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getRelaseDate() {
		return relaseDate;
	}

	public void setRelaseDate(LocalDate relaseDate) {
		this.relaseDate = relaseDate;
	}
	public void setRelaseDate(String relaseDate) {
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		this.relaseDate = LocalDate.parse(relaseDate, formatters);	
	}
	public int getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
